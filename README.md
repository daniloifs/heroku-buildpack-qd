# Heroku buildpack: MongoDB

This is a [Heroku buildpack](http://devcenter.heroku.com/articles/buildpacks) to run mongo commands (http://www.mongodb.org/).

It installs MongoDB 3.4.10  https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-ubuntu1404-3.4.10.tgz.

Usage
-----

Example usage:

    $ heroku create myapp --buildpack https://daniloifs@bitbucket.org/daniloifs/heroku-buildpack-mongo.git
    $ git push heroku master
    
or:

    $ heroku buildpacks:add 
    https://daniloifs@bitbucket.org/daniloifs/heroku-buildpack-mongo.